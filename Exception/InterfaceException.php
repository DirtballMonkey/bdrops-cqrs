<?php

declare(strict_types=1);

namespace Bdrops\CQRS\Exception;

class InterfaceException extends \Exception implements ExceptionInterface
{
    public function __construct($message = '', $code = CODE_ERROR, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}

<?php

declare(strict_types=1);

namespace Bdrops\CQRS\Handler;

use Bdrops\CQRS\Exception\InterfaceException;
use Bdrops\CQRS\Interfaces\AggregateInterface;
use Bdrops\CQRS\Interfaces\CommandInterface;
use Bdrops\CQRS\Interfaces\EventInterface;
use Bdrops\CQRS\Message\Message;
use Bdrops\CQRS\Services\AggregateFactory;
use Bdrops\CQRS\Services\MessageBus;

abstract class Handler
{
    /** @var MessageBus|null */
    public $messageBus;

    /** @var AggregateFactory|null */
    public $aggregateFactory;

    /**
     * Handler constructor.
     *
     * @param MessageBus       $messageBus
     * @param AggregateFactory $aggregateFactory
     */
    public function __construct(MessageBus $messageBus, AggregateFactory $aggregateFactory)
    {
        $this->messageBus = $messageBus;
        $this->aggregateFactory = $aggregateFactory;
    }

    /**
     * Returns the Command class associated with this Handler.
     *
     * @return string
     */
    abstract public static function getCommandClass(): string;

    /**
     * Returns a new Event instance of the Event class associated with this Handler.
     *
     * @param CommandInterface $command
     *
     * @return EventInterface
     */
    abstract public function createEvent(CommandInterface $command): EventInterface;

    /**
     * Returns true if the Command is valid, false otherwise.
     *
     * @param CommandInterface   $command
     * @param AggregateInterface $aggregate
     *
     * @return bool
     */
    abstract public function validateCommand(CommandInterface $command, AggregateInterface $aggregate): bool;

    /**
     * Executes the business logic this Handler implements.
     *
     * @param CommandInterface   $command
     * @param AggregateInterface $aggregate
     *
     * @return AggregateInterface
     */
    abstract public function execute(CommandInterface $command, AggregateInterface $aggregate): AggregateInterface;

    /**
     * A wrapper for the execute function.
     *
     * @param CommandInterface   $command
     * @param AggregateInterface $aggregate
     *
     * @return AggregateInterface
     */
    public function executeHandler(CommandInterface $command, AggregateInterface $aggregate): AggregateInterface
    {
        /* Execute method is implemented in final class */
        return $this->execute($command, $aggregate);
    }

    /**
     * Returns an Aggregate based on the provided uuid.
     *
     * @param string $uuid
     * @param string $aggregateClass
     * @param int    $user
     *
     * @return AggregateInterface
     */
    public function getAggregate(string $uuid, string $aggregateClass, int $user): AggregateInterface
    {
        $aggregate = $this->aggregateFactory->build($uuid, $aggregateClass, null, $user);

        return $aggregate;
    }

    /**
     * When the Handler is invokes it performs the following actions:
     * Get the Aggregate.
     * Check if the Command is valid.
     * Create an Event for the Command and push it onto the Aggregates pending Events.
     *
     * @param CommandInterface $command
     * @param array            $aggregates
     */
    public function __invoke(CommandInterface $command, array &$aggregates): void
    {
        $aggregateClass = $command->getAggregateClass();
        $aggregateUuid = $command->getAggregateUuid();
        $user = $command->getUser();

        // Get Aggregate.
        $aggregate = $this->getAggregate($aggregateUuid, $aggregateClass, $user);

        // Check if commands input is valid.
        $validCommand = $this->validateCommand($command, $aggregate);

        // Check if the Aggregate and target Version matches.
        $versionMatches = $aggregate->getVersion() === $command->getOnVersion();

        if (!$versionMatches) {
            // Version does not match.
            $this->messageBus->dispatch(new Message(
                'Aggregate target version is outdated or does not exist',
                CODE_CONFLICT,
                $command->getUuid(),
                $aggregateUuid,
                null
            ));
        } elseif ($validCommand) {
            try {
                // Create Event for Command.
                $event = $this->createEvent($command);

                if ($event instanceof EventInterface) {
                    // Apply Event to Aggregate.
                    $aggregate = $this->aggregateFactory->apply($aggregate, $event);

                    $aggregates[] = $aggregate;
                } else {
                    throw new InterfaceException(get_class($event).' must implement '.EventInterface::class);
                }
            } catch (InterfaceException $e) {
                $this->messageBus->dispatch(new Message(
                    $e->getMessage(),
                    $e->getCode(),
                    $command->getUuid(),
                    $aggregateUuid,
                    $e
                ));
            }
        } else {
            // Handle invalid command.
            $this->messageBus->dispatch(new Message(
                'Invalid Command',
                CODE_BAD_REQUEST,
                $command->getUuid(),
                $aggregateUuid,
                null
            ));
        }
    }
}

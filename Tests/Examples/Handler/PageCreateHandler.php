<?php

declare(strict_types=1);

namespace Bdrops\CQRS\Tests\Examples\Handler;

use Bdrops\CQRS\Handler\Handler;
use Bdrops\CQRS\Tests\Examples\Command\PageCreateCommand;
use Bdrops\CQRS\Tests\Examples\Event\PageCreateEvent;
use Bdrops\CQRS\Tests\Examples\Model\Page;
use Bdrops\CQRS\Interfaces\AggregateInterface;
use Bdrops\CQRS\Interfaces\CommandInterface;
use Bdrops\CQRS\Interfaces\EventInterface;
use Bdrops\CQRS\Interfaces\HandlerInterface;
use Bdrops\CQRS\Message\Message;

final class PageCreateHandler extends Handler implements HandlerInterface
{
    /**
     * {@inheritdoc}
     *
     * @var Page $aggregate
     */
    public function execute(CommandInterface $command, AggregateInterface $aggregate): AggregateInterface
    {
        $payload = $command->getPayload();

        $aggregate->title = $payload['title'];

        return $aggregate;
    }

    /**
     * {@inheritdoc}
     */
    public static function getCommandClass(): string
    {
        return PageCreateCommand::class;
    }

    /**
     * {@inheritdoc}
     */
    public function createEvent(CommandInterface $command): EventInterface
    {
        return new PageCreateEvent($command);
    }

    /**
     * {@inheritdoc}
     */
    public function validateCommand(CommandInterface $command, AggregateInterface $aggregate): bool
    {
        $payload = $command->getPayload();

        if (0 === $aggregate->getVersion() && isset($payload['title']) && !empty($payload['title'])) {
            return true;
        }
        if (0 !== $aggregate->getVersion()) {
            $this->messageBus->dispatch(new Message(
                'Aggregate already exists',
                CODE_CONFLICT,
                $command->getUuid(),
                $command->getAggregateUuid()
            ));

            return false;
        } else {
            $this->messageBus->dispatch(new Message(
                'You must enter a title',
                CODE_BAD_REQUEST,
                $command->getUuid(),
                $command->getAggregateUuid()
            ));

            return false;
        }
    }
}

<?php

declare(strict_types=1);

namespace Bdrops\CQRS\Tests\Examples\Model;

use Bdrops\CQRS\Model\Aggregate;

final class Page extends Aggregate
{
    /** @var string */
    public $title;
}

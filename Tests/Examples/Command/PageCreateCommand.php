<?php

declare(strict_types=1);

namespace Bdrops\CQRS\Tests\Examples\Command;

use Bdrops\CQRS\Tests\Examples\Handler\PageCreateHandler;
use Bdrops\CQRS\Tests\Examples\Model\Page;
use Bdrops\CQRS\Command\Command;
use Bdrops\CQRS\Interfaces\CommandInterface;

class PageCreateCommand extends Command implements CommandInterface
{
    /**
     * {@inheritdoc}
     */
    public function getHandlerClass(): string
    {
        return PageCreateHandler::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getAggregateClass(): string
    {
        return Page::class;
    }
}

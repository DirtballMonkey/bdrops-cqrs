<?php

declare(strict_types=1);

namespace Bdrops\CQRS\Tests\Examples\Listener;

use Bdrops\CQRS\Interfaces\EventInterface;
use Bdrops\CQRS\Interfaces\ListenerInterface;
use Bdrops\CQRS\Services\CommandBus;

class PageCreateListener implements ListenerInterface
{
    /**
     * {@inheritdoc}
     */
    public function __invoke(CommandBus $commandBus, EventInterface $event): void
    {
    }
}

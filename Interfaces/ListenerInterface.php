<?php

declare(strict_types=1);

namespace Bdrops\CQRS\Interfaces;

use Bdrops\CQRS\Services\CommandBus;

interface ListenerInterface
{
    /**
     * This function is called when the Listener is dispatched.
     * Implement business logic here.
     *
     * @param CommandBus     $commandBus
     * @param EventInterface $event
     */
    public function __invoke(CommandBus $commandBus, EventInterface $event): void;
}
